function getMainChunk(stats) {
  return (
    stats.chunks.find((x) => x.names.includes("main")) || {
      id: 0,
      size: 0,
      files: [],
    }
  );
}

module.exports = function entryPointAnalysis(stats) {
  if (!stats) {
    return null;
  }

  // Consider the main chunk to be special thing
  const { id: mainChunkID, size: mainChunkSize, files } = getMainChunk(stats);

  // Exclude the main chunk from the entrypoint sizes.
  const memoizedChunkSize = { [mainChunkID.toString()]: 0 };

  const result = {
    mainChunk: { type: "special", name: "main", size: mainChunkSize, files },
    average: { type: "special", name: "average", size: 0, count: 0 },
  };

  Object.entries(stats.entrypoints).map(([key, entry]) => {
    const { chunks, assets, ...rest } = entry;

    let size = 0;

    chunks.forEach((id) => {
      const key = id.toString();
      if (!Number.isFinite(memoizedChunkSize[key])) {
        const chunk = stats.chunks.find((x) => x.id === id);

        memoizedChunkSize[key] = chunk.size;
      }
      size += memoizedChunkSize[key];
    });

    result.average.size += size;
    result.average.count += 1;

    result[key] = {
      name: key,
      type: "entrypoint",
      size,
      assets: assets.filter((x) => !x.endsWith(".map")),
      ...rest,
    };
  });

  result.average.size =
    Math.round(result.average.size / result.average.count) + mainChunkSize;

  return result;
};
