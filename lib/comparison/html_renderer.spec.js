const compareReports = require("./compare_reports");
const HTMLRenderer = require("./html_renderer");

const prettier = require("prettier");

describe("comparison", () => {
  const from = require("../__fixtures__/entry_points_before.json");
  const to = require("../__fixtures__/entry_points_after.json");

  const comparison = compareReports(from, to);
  it("compareReports matches HTML", () => {
    const html = prettier.format(HTMLRenderer(comparison), { parser: "html" });

    expect(html).toMatchSnapshot();
  });
});
