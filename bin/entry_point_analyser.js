#!/usr/bin/env node

const argumentsParser = require("commander");

const entryPointAnalysis = require("../lib/entry_point_analysis.js");
const { readGargantuanJSON, writeJSON } = require("../lib/file_utils");

const args = argumentsParser
  .option("--from-file <file>", "Source webpack report (file)")
  .option("--sha <sha>", "Commit sha for additional metadata")
  .option("--json <json>", "Output to json file")
  .parse(process.argv);

async function main() {
  if (!(args.fromFile && args.json)) {
    console.error("Please provide --from-file and --json");
    args.outputHelp();
    throw new Error("Arguments missing");
  }
  let results = entryPointAnalysis(await readGargantuanJSON(args.fromFile));

  if (args.sha) {
    const { gitlabAPI } = require("../lib/common");
    const { data: commit } = await gitlabAPI(`/repository/commits/${args.sha}`);
    results = { ...commit, statistics: results };
  }

  writeJSON(args.json, results);
}

main().catch((e) => {
  console.error(e);
  process.exit(1);
});
